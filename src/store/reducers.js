export const initialState = {
  loading: true,
  movie: {},
  discover: {},
  movieCredits: {},
  person: {},
  personKnown: {},
  errorMessage: null,
};

export const reducer = (state, action) => {
  switch (action.type) {
    /** Person Known / Credits */
    case 'PERSON_KNOWN_REQUEST':
      return {
        ...state,
        loading: true,
        errorMessage: null,
      };
    case 'PERSON_KNOWN_SUCCESS':
      return {
        ...state,
        loading: false,
        personKnown: action.payload,
      };
    case 'PERSON_KNOWN_FAILURE':
      return {
        ...state,
        loading: false,
        errorMessage: 'Person data error.',
      };

    /** Person */
    case 'PERSON_DATA_REQUEST':
      return {
        ...state,
        loading: true,
        errorMessage: null,
      };
    case 'PERSON_DATA_SUCCESS':
      return {
        ...state,
        loading: false,
        person: action.payload,
      };
    case 'PERSON_DATA_FAILURE':
      return {
        ...state,
        loading: false,
        errorMessage: 'Person data error.',
      };

    /** Discover Movie */
    case 'DISCOVER_MOVIE_REQUEST':
      return {
        ...state,
        loading: true,
        errorMessage: null,
      };
    case 'DISCOVER_MOVIE_SUCCESS':
      return {
        ...state,
        loading: false,
        discover: action.payload,
      };
    case 'DISCOVER_MOVIE_FAILURE':
      return {
        ...state,
        loading: false,
        errorMessage: 'Discover movie error.',
      };

    /** Search for Movie */
    case 'SEARCH_MOVIE_REQUEST':
      return {
        ...state,
        loading: true,
        errorMessage: null,
      };
    case 'SEARCH_MOVIE_SUCCESS':
      return {
        ...state,
        loading: false,
        movie: action.payload,
      };
    case 'SEARCH_MOVIE_FAILURE':
      return {
        ...state,
        loading: false,
        errorMessage: 'Search error.',
      };

    /** Movie Details */
    case 'MOVIE_DETAILS_REQUEST':
      return {
        ...state,
        loading: true,
        errorMessage: null,
      };
    case 'MOVIE_DETAILS_SUCCESS':
      return {
        ...state,
        loading: false,
        movie: action.payload,
      };
    case 'MOVIE_DETAILS_FAILURE':
      return {
        ...state,
        loading: false,
        errorMessage: 'Error to get movie detail.',
      };

    /** Movie Credits */
    case 'MOVIE_CREDITS_REQUEST':
      return { ...state, loading: true, errorMessage: null };
    case 'MOVIE_CREDITS_SUCCESS':
      return {
        ...state,
        loading: false,
        movieCredits: action.payload,
      };
    case 'MOVIE_CREDITS_FAILURE':
      return {
        ...state,
        loading: false,
        errorMessage: 'Movie credits is not available.',
      };
    default:
      return state;
  }
};
