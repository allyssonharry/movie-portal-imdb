import React from 'react';
import ContentLoader from 'react-content-loader';

/** Person Page Loader */
export const PersonPageLoader = () => (
  <ContentLoader height={475} width={400}>
    <circle cx="30" cy="30" r="30" />

    <rect x="75" y="13" rx="4" ry="4" width="100" height="13" />
    <rect x="75" y="37" rx="4" ry="4" width="50" height="8" />
    <rect x="0" y="70" rx="5" ry="5" width="400" height="400" />
  </ContentLoader>
);

/** Movie Page Loader */
export const MoviePageLoader = props => {
  return (
    <ContentLoader
      height={600}
      width={400}
      speed={3}
      primaryColor="#d9d9d9"
      secondaryColor="#ecebeb"
      {...props}
    >
      <rect x="20" y="8" rx="0" ry="0" width="100" height="100" />
      <rect x="20" y="120" rx="0" ry="0" width="100" height="10" />
      <rect x="170" y="8" rx="0" ry="0" width="300" height="15" />
      <rect x="170" y="30" rx="0" ry="0" width="300" height="15" />
      <rect x="170" y="52" rx="0" ry="0" width="100" height="15" />
    </ContentLoader>
  );
};

/** Avatar Loader */
export const AvatarLoader = props => {
  return (
    <ContentLoader
      height={280}
      width={500}
      speed={3}
      primaryColor="#d9d9d9"
      secondaryColor="#ecebeb"
      {...props}
    >
      <circle cx="70" cy="50" r="30" />
      <rect x="0" y="90" rx="0" ry="0" width="140" height="25" />
      <circle cx="230" cy="50" r="30" />
      <rect x="160" y="90" rx="0" ry="0" width="140" height="25" />
      <circle cx="390" cy="50" r="30" />
      <rect x="320" y="90" rx="0" ry="0" width="140" height="25" />
    </ContentLoader>
  );
};
