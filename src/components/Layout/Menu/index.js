import React from 'react';
import { NavLink } from 'react-router-dom';
import './style.css';

const Menu = () => (
  <div className="nav-scroller py-1 mb-2">
    <nav className="nav d-flex justify-content-between">
      <NavLink className="p-2 text-muted" to="/">
        Discover
      </NavLink>
      <NavLink className="p-2 text-muted" to="#">
        Movies
      </NavLink>
      <NavLink className="p-2 text-muted" to="#">
        TV Series
      </NavLink>
      <NavLink className="p-2 text-muted" to="#">
        Top Rated
      </NavLink>
      <NavLink className="p-2 text-muted" to="#">
        Upcoming
      </NavLink>
      <NavLink className="p-2 text-muted" to="#">
        Now Playing
      </NavLink>
      <NavLink className="p-2 text-muted" to="#">
        Person
      </NavLink>
    </nav>
  </div>
);

export default Menu;
