import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Menu from '../Menu';
import Search from '../../Search';
import './style.css';

const Header = ({ title }) => (
  <>
    <header className="site-header py-3">
      <Container>
        <div className="row flex-nowrap justify-content-between align-items-center">
          <div className="col-4 pt-1">
            <Link to="/">{title}</Link>
          </div>
          <div className="col-4 text-center">
            <Search />
          </div>
          <div className="col-4 d-flex justify-content-end align-items-center">
            <Link className="btn btn-sm btn-outline-secondary" to="/register">
              Sign up
            </Link>
          </div>
        </div>
      </Container>
    </header>
    <Container>
      <Menu />
    </Container>
  </>
);

export default Header;
