import React, { useReducer } from 'react';
import credentials from '../../config';
import { api } from '../../services/api';
import { initialState, reducer } from '../../store/reducers';
import FormSearch from './form';

const Search = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const searchResults = async value => {
    dispatch({
      type: 'SEARCH_MOVIE_REQUEST',
    });

    await api
      .get(
        `/search/movie?language=pt-BR&query=${value}&page=1&include_adult=false&api_key=${credentials.api_key}`
      )
      .then(response => {
        if (response) {
          dispatch({
            type: 'SEARCH_MOVIE_SUCCESS',
            payload: response.data,
          });
        } else {
          dispatch({
            type: 'SEARCH_MOVIE_FAILURE',
          });
        }
      });
  };

  const {
    movie: { results },
  } = state;

  return (
    <>
      <FormSearch search={searchResults} />
      {results ? (
        <div
          style={{
            position: 'absolute',
            zIndex: '999',
            backgroundColor: 'white',
            padding: '1rem',
            boxShadow: '1px 1px 5px #888',
            borderRadius: '0 0 4px 4px',
          }}
        >
          <ul
            style={{ listStyle: 'none', textAlign: 'left', marginBottom: '0' }}
          >
            {results.map(result => (
              <li key={result.id}>{result.title}</li>
            ))}
          </ul>
        </div>
      ) : null}
    </>
  );
};

export default Search;
