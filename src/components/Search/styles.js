import styled from 'styled-components';

export const Form = styled.form`
  flex: 1 1 auto;
  min-height: 0;
  input {
    font-size: 16px;
    font-weight: normal;
    height: 100%;
    outline: none;
    padding: 0;
    width: 100%;
  }
`;
