import React, { useState } from 'react';
import { Form, FormControl, Button } from 'react-bootstrap';

const FormSearch = ({ search }) => {
  const [searchValue, setSearchValue] = useState('');

  const handleSearchInputChanges = e => {
    const { value } = e.target;
    setSearchValue(value);
  };

  const callSearchFunction = e => {
    e.preventDefault();
    search(searchValue);
  };

  return (
    <Form inline>
      <FormControl
        value={searchValue}
        onChange={handleSearchInputChanges}
        type="search"
        placeholder="Type to search..."
        className="mr-sm-2"
      />
      <Button
        type="submit"
        onClick={callSearchFunction}
        variant="outline-success"
      >
        Search
      </Button>
    </Form>
  );
};

export default FormSearch;
