import React from 'react';
import { Container } from 'react-bootstrap';

const Hello = () => (
  <Container>
    <h1>Hello Component</h1>
  </Container>
);

export default Hello;
