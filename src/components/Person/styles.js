import styled from 'styled-components';

export const Header = styled.header`
  align-items: stretch;
  border: 0;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  flex-shrink: 0;
  margin: 0;
  padding: 0;
  position: relative;

  @media (min-width: 736px) {
    margin-bottom: 44px;
  }

  /** Cover Column Styles */
  > div {
    flex-shrink: 0;
    @media (min-width: 768px) {
      flex-basis: 0;
      flex-grow: 1;
      margin-right: 30px;
    }

    > span {
      height: 450px;
      width: 300px;

      box-sizing: border-box;
      display: block;
      flex: 0 0 auto;
      overflow: hidden;
      position: relative;

      > img {
        height: 100%;
        width: 100%;
        -webkit-touch-callout: none;
      }
    }
  }

  /** Section Column Styles */
  section {
    color: #262626;
    flex-shrink: 1;
    min-width: 0;

    @media (min-width: 736px) {
      flex-basis: 30px;
      flex-grow: 2;
    }

    > div {
      display: flex;
      align-items: center;
      flex-direction: row;

      @media (min-width: 736px) {
        margin-bottom: 20px;
      }

      h1 {
        font-size: 26px;
        font-weight: 300;
        display: block;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;

        > span {
          font-size: 80%;
          color: #999;
        }
      }

      div {
        flex-shrink: 0;
        margin-left: 20px;
      }
    }

    /** Movie Reviews ~ Votes ~ Coverage */
    > ul {
      margin: 0;
      padding: 0;
      list-style-type: none;
      display: flex;
      flex-direction: row;
      margin-bottom: 20px;

      li {
        font-size: 16px;
        margin-right: 40px;

        &:first-child {
          margin-left: 0;
        }
      }
    }
    /** Movie Description ~ Overview */
    > article {
      display: block;

      @media (min-width: 736px) {
        font-size: 16px;
        line-height: 24px;
        word-wrap: break-word;
      }

      h3 {
        margin-bottom: 8px;
        font-weight: bold;
      }

      ul {
        margin: 0 0 20px;
        padding: 0;
        list-style-type: none;
      }
    }
  }
`;

export const Tab = styled.div`
  align-items: center;
  color: #999;
  display: flex;
  flex-direction: row;
  font-size: 12px;
  font-weight: 600;
  justify-content: center;
  letter-spacing: 1px;
  text-align: center;
  border-top: 1px solid #efefef;

  > a {
    cursor: pointer;
    display: flex;
    flex-direction: row;
    height: 52px;
    justify-content: center;
    text-transform: uppercase;
    align-items: center;

    &:not(:last-child) {
      @media (min-width: 736px) {
        margin-right: 60px;
      }
    }

    &:visited {
    }
  }
`;

export const TabContent = styled.div``;
