import React, { useEffect, useReducer } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import credentials from '../../config';
import { api } from '../../services/api';
import { initialState, reducer } from '../../store/reducers';
import { PersonPageLoader } from '../Loaders';
import PersonKnown from '../PersonKnown';

import { Header, Tab, TabContent } from './styles';

const Person = ({ history }) => {
  /** Get Person ID from URI */
  const { id } = useParams();

  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    api
      .get(`/person/${id}?language=pt-BR&api_key=${credentials.api_key}`)
      .then(response => {
        dispatch({
          type: 'PERSON_DATA_SUCCESS',
          payload: response.data,
        });
      });
  }, [id]);

  const { loading, person } = state;

  const {
    location: { pathname },
  } = history;

  const DEFAULT_BASE_IMAGE_URL = `https://image.tmdb.org/t/p/w300_and_h450_bestv2`;

  return (
    <Container>
      {loading ? (
        <PersonPageLoader />
      ) : (
        <>
          <Header>
            <div>
              <span>
                <img
                  src={`${DEFAULT_BASE_IMAGE_URL}${person.profile_path}`}
                  alt={person.name || 'John Doe'}
                />
              </span>
            </div>
            <section>
              <div>
                <h1>
                  {person.name} <span>({person.also_known_as[0]})</span>
                </h1>
                {/* <div>(Button Action)</div> */}
              </div>

              <ul>
                <li>
                  birthday <strong>{person.birthday}</strong>
                </li>
                <li>
                  burn <strong>{person.place_of_birth}</strong>
                </li>
                <li>
                  coverage <strong>{person.popularity}</strong>
                </li>
              </ul>

              <article>
                <ul>
                  {/* <li>Diretor(a):</li>
              <li>Diretor(a):</li>
              <li>Diretor(a):</li> */}
                </ul>
                <section>
                  <h3>Biography</h3>
                  <p>{person.biography || 'No data...'}</p>
                </section>

                <PersonKnown personId={id} />
              </article>
            </section>
          </Header>
          <Tab>
            <Link to={`/person/${id}`}>Timeline</Link>
            <Link to={`/person/${id}/known_as`}>Known As</Link>
            <Link to={`/person/${id}/photos`}>Photos</Link>
            <Link to={`/person/${id}/reviews`}>Reviews</Link>
          </Tab>
          {pathname.startsWith(`/person/${id}`) ? (
            <TabContent>{person.overview}</TabContent>
          ) : (
            <TabContent>Outra Tab</TabContent>
          )}
        </>
      )}
    </Container>
  );
};

export default Person;
