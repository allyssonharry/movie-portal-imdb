import React from 'react';

import { Container } from '../../styles/layout';

const PageNotFound = () => (
  <Container>
    <h1>Page Not Found</h1>
  </Container>
);

export default PageNotFound;
