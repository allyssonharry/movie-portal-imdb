/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useReducer } from 'react';
import Carousel from 'react-slick';
import credentials from '../../config';
import { api } from '../../services/api';
import { initialState, reducer } from '../../store/reducers';
import { AvatarLoader } from '../Loaders';
import { MovieItem } from './styles';

const PersonKnown = ({ personId }) => {
  /** Main State */
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    api
      .get(`/person/${personId}/movie_credits?api_key=${credentials.api_key}`)
      .then(response => {
        dispatch({
          type: 'PERSON_KNOWN_SUCCESS',
          payload: response.data,
        });
      });
  }, [personId]);

  const {
    loading,
    personKnown: { cast, crew },
  } = state;

  const IMAGE_BASE_URL = 'https://image.tmdb.org/t/p/w276_and_h350_face';

  return (
    <>
      <div>
        {loading ? (
          <AvatarLoader />
        ) : (
          <>
            <h1>Known As</h1>
            <Carousel
              {...{
                dots: false,
                arrows: false,
                infinite: false,
                speed: 500,
                slidesToShow: 6,
                slidesToScroll: 2,
              }}
            >
              {cast.map(known => (
                <div key={known.id}>
                  <MovieItem to={`/movie/${known.id}`}>
                    <div>
                      <img
                        src={`${IMAGE_BASE_URL}${known.poster_path}`}
                        alt={known.name}
                      />
                    </div>
                    <section>
                      <h3>{known.name}</h3>
                      <p>{known.character}</p>
                    </section>
                  </MovieItem>
                </div>
              ))}
            </Carousel>
            <h1>Crew</h1>
            <Carousel
              {...{
                dots: false,
                arrows: false,
                infinite: false,
                speed: 500,
                slidesToShow: 6,
                slidesToScroll: 2,
              }}
            >
              {crew.map(known => (
                <div key={known.id}>
                  <div to={`/known/${known.id}`}>
                    <div>
                      <img
                        src={`${IMAGE_BASE_URL}${known.profile_path}`}
                        alt={known.name}
                      />
                    </div>
                    <section>
                      <h3>{known.name}</h3>
                      <p>{known.character}</p>
                    </section>
                  </div>
                </div>
              ))}
            </Carousel>
          </>
        )}
      </div>
    </>
  );
};

export default PersonKnown;
