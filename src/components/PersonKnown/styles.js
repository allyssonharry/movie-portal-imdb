import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const MovieItem = styled(Link)`
  position: relative;
  display: flex;
  flex-direction: column;
  min-width: 0;
  word-wrap: break-word;
  background: none;
  background-clip: border-box;
  border: 0;
  border-radius: 0.25rem;
  text-decoration: none;

  > div {
    width: 90px;
    height: 90px;
    border-radius: 10%;
    overflow: hidden;
    position: relative;
    margin: 0 auto;
    border: 1px solid #dadada;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }

  > section {
    margin-top: 10px;
    text-align: center;
    line-height: 1.4;

    h3 {
      display: block;
      cursor: pointer;
      overflow: hidden;
      text-align: center;
      text-overflow: ellipsis;
      white-space: nowrap;
      width: 100%;
      margin-bottom: 0;
      font-size: 14px;
    }

    p {
      font-size: 80%;
      font-weight: 300;
    }
  }
`;
