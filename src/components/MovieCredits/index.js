/* eslint-disable react/jsx-props-no-spreading */
import PropTypes from 'prop-types';
import React, { useEffect, useReducer } from 'react';
import Carousel from 'react-slick';
import credentials from '../../config';
import { api } from '../../services/api';
import { initialState, reducer } from '../../store/reducers';
import { AvatarLoader } from '../Loaders';
import { Person } from './styles';

const MovieCredits = ({ movieId }) => {
  /** Main State */
  const [state, dispatch] = useReducer(reducer, initialState);

  // componentDidMount()
  useEffect(() => {
    api
      .get(`/movie/${movieId}/credits?api_key=${credentials.api_key}`)
      .then(response => {
        dispatch({
          type: 'MOVIE_CREDITS_SUCCESS',
          payload: response.data,
        });
      });
  }, [movieId]);

  const {
    loading,
    movieCredits: { cast },
  } = state;

  const IMAGE_BASE_URL = 'https://image.tmdb.org/t/p/w276_and_h350_face';

  return (
    <>
      <div>
        {loading ? (
          <AvatarLoader />
        ) : (
          <>
            <h1>Elenco</h1>
            <Carousel
              {...{
                dots: false,
                arrows: false,
                infinite: false,
                speed: 500,
                slidesToShow: 6,
                slidesToScroll: 2,
              }}
            >
              {cast.map(person => (
                <div key={person.id}>
                  <Person to={`/person/${person.id}`}>
                    <div>
                      <img
                        src={`${IMAGE_BASE_URL}${person.profile_path}`}
                        alt={person.name}
                      />
                    </div>
                    <section>
                      <h3>{person.name}</h3>
                      <p>{person.character}</p>
                    </section>
                  </Person>
                </div>
              ))}
            </Carousel>
          </>
        )}
      </div>
    </>
  );
};

MovieCredits.propTypes = {
  movieId: PropTypes.node.isRequired,
};

export default MovieCredits;
