import React, { useEffect, useReducer } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import credentials from '../../config';
import { api } from '../../services/api';
import { initialState, reducer } from '../../store/reducers';
import { MoviePageLoader } from '../Loaders';
import MovieCredits from '../MovieCredits';

const Movie = ({ history }) => {
  /** State */
  const [state, dispatch] = useReducer(reducer, initialState);

  /* Get Movie ID */
  const { id } = useParams();

  /** Instead of componentDidMount() */
  useEffect(() => {
    api
      .get(`/movie/${id}?language=pt-BR&api_key=${credentials.api_key}`)
      .then(response => {
        dispatch({
          type: 'MOVIE_DETAILS_SUCCESS',
          payload: response.data,
        });
      });
  }, [id]);

  /** Destruct */
  const { loading, movie } = state;

  const {
    location: { pathname },
  } = history;

  // console.log(movies);
  const DEFAULT_BASE_IMAGE_URL = `https://image.tmdb.org/t/p/w300_and_h450_bestv2`;

  return (
    <>
      <Container>
        {loading ? (
          <MoviePageLoader />
        ) : (
          <>
            <header>
              <div>
                <span>
                  <img
                    src={`${DEFAULT_BASE_IMAGE_URL}${movie.poster_path}`}
                    alt={movie.title || movie.original_title}
                  />
                </span>
              </div>
              <section>
                <div>
                  <h1>
                    {movie.title} <span>{movie.release_date}</span>
                  </h1>
                  <div>(Button Action)</div>
                </div>

                <ul>
                  <li>
                    <strong>{movie.vote_average}</strong> avaliações
                  </li>
                  <li>
                    <strong>{movie.vote_count}</strong> popularidade
                  </li>
                  <li>
                    <strong>{movie.popularity}</strong> coverage
                  </li>
                </ul>

                <article>
                  <ul>
                    <li>Título original: {movie.original_title}</li>
                    {/* <li>Diretor(a):</li>
                  <li>Diretor(a):</li>
                  <li>Diretor(a):</li> */}
                  </ul>
                  <section>
                    <h3>Sipnose</h3>
                    <p>{movie.overview}</p>
                  </section>

                  <MovieCredits movieId={id} />
                </article>
              </section>
            </header>
            <div>
              <Link to={`/movie/${id}`}>Sinopse</Link>
              <Link to={`/movie/${id}/credits`}>Prodrução</Link>
              <Link to={`/movie/${id}/reviews`}>Reviews</Link>
            </div>
            {pathname.startsWith(`/movie/${id}`) ? (
              <div>{movie.overview}</div>
            ) : (
              <div>Outra Tab</div>
            )}
          </>
        )}
      </Container>
    </>
  );
};

export default Movie;
