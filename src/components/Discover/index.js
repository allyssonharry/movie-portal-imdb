import React, { useEffect, useReducer } from 'react';
import { Link } from 'react-router-dom';
import { Col, Card } from 'react-bootstrap';
import credentials from '../../config';
import { api } from '../../services/api';
import { initialState, reducer } from '../../store/reducers';

const Discover = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const discoverParams = `&language=pt-BR&sort_by=popularity.desc&include_adult=false&include_video=true&page=1`;

  useEffect(() => {
    api
      .get(`/discover/movie?api_key=${credentials.api_key}${discoverParams}`)
      .then(response => {
        dispatch({
          type: 'DISCOVER_MOVIE_SUCCESS',
          payload: response.data,
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  }, [discoverParams]);

  const DEFAULT_BASE_IMAGE_URL =
    'https://image.tmdb.org/t/p/w300_and_h450_bestv2';

  const { loading, discover } = state;

  return loading ? (
    <h4 style={{ textAlign: 'center' }}>Carregando...</h4>
  ) : (
    discover.results.map(movie => (
      <Col xs={6} md={4} lg={3} key={movie.id}>
        <Link to={`/movie/${movie.id}`}>
          <Card>
            <Card.Img
              variant="top"
              src={`${DEFAULT_BASE_IMAGE_URL}${movie.poster_path}`}
              alt={`The movie titled: ${movie.title}`}
            />
            <Card.Body>
              <Card.Title>{movie.title}</Card.Title>
              <Card.Text>Popularidade: {movie.popularity}</Card.Text>
              <Card.Text>Votos: {movie.vote_count}</Card.Text>
              <Card.Text>Avaliação: {movie.vote_average}</Card.Text>
              <Card.Text>
                <time>{movie.release_date}</time>
              </Card.Text>
            </Card.Body>
          </Card>
        </Link>
      </Col>
    ))
  );
};

export default Discover;
