import React from 'react';
import { Router } from 'react-router-dom';
import Header from './components/Layout/Header';
import Routes from './routes';
import history from './services/history';

const App = () => {
  return (
    <Router history={history}>
      <Header title="Branding" />
      <Routes />
    </Router>
  );
};

export default App;
