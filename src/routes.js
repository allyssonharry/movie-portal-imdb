import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Movie from './components/Movie';
import Person from './components/Person';
import HomePage from './pages/HomePage';

import Hello from './components/Hello';

const Routes = () => {
  return (
    <Switch>
      {/* Page Routes */}
      <Route exact path="/" component={HomePage} />
      {/* Params Routes Only */}
      <Route exact path="/movie" component={Hello} />
      <Route path="/movie/:id" component={Movie} />
      <Route exact path="/person" component={Hello} />
      <Route path="/person/:id" component={Person} />
    </Switch>
  );
};

export default Routes;
