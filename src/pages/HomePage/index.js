import React from 'react';
import { Container, Row } from 'react-bootstrap';
import Discover from '../../components/Discover';

const HomePage = () => {
  return (
    <Container>
      <Row>
        <Discover />
      </Row>
    </Container>
  );
};

export default HomePage;
