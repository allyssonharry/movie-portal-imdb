import React from 'react';
/** Discover Movies Component */
import Movie from '../../components/Movie';

const MoviePage = () => {
  return (
    <>
      <h1>Movie Page</h1>
      <Movie />
    </>
  );
};

export default MoviePage;
