import React from 'react';
import Person from '../../components/Person';

const PersonPage = () => {
  return (
    <>
      <h1>Person Page</h1>
      <Person />
    </>
  );
};

export default PersonPage;
